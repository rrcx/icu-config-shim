
# icu-config shim #

Since some version of [ICU](https://icu.unicode.org/) (presumably 60.x) the packages in 
Debian- and Ubuntu-based systems are missing `icu-config` script.

The `icu-config` is a simple script that provides a number of build-time relevant info about the
ICU packages. It is superseded by `pkg-config` and sane build environments, but is nevertheless
still used in some project makefiles ([LTFS](https://github.com/LinearTapeFileSystem/ltfs) 
for example).

Fixing the makefiles is tedious, especially for those made with the use of automake/autoconf.

This simple shim script serves as a drop-in replacement for the missing `icu-config`.
It mimics the original interface as much as feasible.


## Installation ##

**System-wide install into /usr/bin (not recommended!)**

1. Put it somewhere into your system
2. `chmod a+x "$ICU_CONFIG_SHIM_ABS_PATH"`
3. `ln -s "$ICU_CONFIG_SHIM_ABS_PATH" /usr/bin/icu-config`

(replace `$ICU_CONFIG_SHIM_ABS_PATH` with the absolute path to where you put it)


**PATH= install**

1. Put it somewhere into your system
2. `chmod a+x "$ICU_CONFIG_SHIM_ABS_PATH"`
3. `export PATH="$(dirname $ICU_CONFIG_SHIM_ABS_PATH):$PATH"`

(replace `$ICU_CONFIG_SHIM_ABS_PATH` with the absolute path to where you put it)


## License ##

This script is rather trivial.

icu-config-shim is made available under the terms and conditions of CC0 Public Domain Dedication
available in [LICENSE](./LICENSE) file and 
[on the Creative Commons website](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

To the extent possible under law, the authors and copyright holders for this work - 
Mikhail Konovalov and Diverse Solutions LLC, have waived all copyright and related or neighboring
rights to _icu-config-shim_.
This work is published from: Russian Federation.
